import React from "react";
import "./index.css";
import { useNavigate } from "react-router";

const Login = () => {
  const navigate = useNavigate();

  const handleLogin = () => {
    navigate(`/admin`);
  };

  return (
    <div className="login-page">
      <div className="login-logo">Login</div>
      <form>
        <div className="input-login">
          <label htmlFor="email">Username:</label>
          <input type="email" id="email" required />
        </div>
        <div className="input-login">
          <label htmlFor="password">Password:</label>
          <input type="password" id="password" required />
          <div className="remember-me">
            <input type="checkbox" id="remember" />
            <label htmlFor="remember">Remember Me</label>
          </div>
        </div>
        <button className="login-button" type="submit" onClick={handleLogin}>
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;
