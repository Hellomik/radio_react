import React, { useEffect, useState } from "react";
import "./index.css";
import edit from "../../assets/Pencil (1).png";
import deleteButton from "../../assets/Trash.png";
import MultiSelect from "../../components/MultiSelect";
import axios from "axios";
import { globalHost } from "../..";
import { ChannelEntity } from "../channel_entity";

export interface UserData {
  id: string;
  userName: string;
  name: string;
  email: string;
  expiry: string;
  radioLinks: string[];
  password: string;
}

function Admin() {
  const [showPopup, setShowPopup] = useState(false);
  const [formData, setFormData] = useState<Omit<UserData, "id">>({
    userName: "",
    name: "",
    email: "",
    expiry: "",
    radioLinks: [],
    password: "",
  });
  const [userData, setUserData] = useState<UserData[]>([]);
  const [channelData, setChannelData] = useState<ChannelEntity[]>([]);

  const handleInputChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;
    if (e.target instanceof HTMLSelectElement && e.target.multiple) {
      const options = e.target.options;
      const value: string[] = [];
      for (let i = 0, len = options.length; i < len; i++) {
        if (options[i].selected) {
          value.push(options[i].value);
        }
      }
      setFormData({ ...formData, [name]: value });
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };

  const handleAddUser = async () => {
    const body = {
      userName: formData.userName,
      password: formData.password,
      name: formData.name,
      email: formData.email,
      date: formData.expiry,
      radioLinks: formData.radioLinks,
    };
    const link = globalHost + "/user";
    await axios.post(link, body);
  };

  const handleDeleteUser = (id: string) => {
    setUserData(userData.filter((user) => user.id !== id));
  };

  useEffect(() => {
    const getData = async () => {
      try {
        const link = globalHost + "/channel";
        const responce = await axios.get(link);

        setChannelData(responce.data);
      } catch (e) {
        console.log(e);
      }
    };
    const getUserData = async () => {
      const link = globalHost + "/user";
      const responce = await axios.get(link);
      const userData: UserData[] = responce.data;
      setUserData(userData);
    };

    getUserData();
    getData().catch((e) => console.log(e));
  }, []);

  return (
    <div className="admin-container">
      <div className="admin-title-row">
        <div className="admin-title">Users</div>
        <button className="admin-add" onClick={() => setShowPopup(true)}>
          Добавить
        </button>
      </div>

      <div className="admin-table">
        <div className="header-admin-table">
          <div className="item-header-admin-table" style={{ width: "2.6%" }}>
            ID
          </div>
          <div className="item-header-admin-table" style={{ width: "8.2%" }}>
            Username
          </div>
          <div className="item-header-admin-table" style={{ width: "7.3%" }}>
            Password
          </div>
          <div className="item-header-admin-table" style={{ width: "5.3%" }}>
            Name
          </div>
          <div className="item-header-admin-table" style={{ width: "10.3%" }}>
            Email
          </div>

          <div className="item-header-admin-table" style={{ width: "13%" }}>
            Expiry
          </div>
          <div className="item-header-admin-table" style={{ width: "17.9%" }}>
            Players
          </div>

          <div className="item-header-admin-table" style={{ width: "4.9%" }}>
            Edits
          </div>
        </div>

        {userData.map((user, index) => (
          <div className="row-admin-table" key={index}>
            <div className="item-admin-table" style={{ width: "2.6%" }}>
              {user.id}
            </div>
            <div className="item-admin-table" style={{ width: "8.2%" }}>
              {user.userName}
            </div>
            <div className="item-admin-table" style={{ width: "5.3%" }}>
              {user.name}
            </div>
            <div className="item-admin-table" style={{ width: "10.3%" }}>
              {user.email}
            </div>
            <div className="item-admin-table" style={{ width: "13%" }}>
              {user.expiry}
            </div>
            <div className="item-admin-table" style={{ width: "17.9%" }}>
              {user.radioLinks && user.radioLinks.join(", ")}
            </div>
            {/* <div className="item-admin-table" style={{ width: "7.3%" }}>
              {user.login}
            </div> */}
            <div className="item-admin-table" style={{ width: "7.3%" }}>
              {user.password}
            </div>
            <div className="item-admin-table" style={{ width: "4.9%" }}>
              <div className="item-admin-table-buttons">
                <button className="item-admin-table-button">
                  <img src={edit} />
                </button>
                <button
                  onClick={() => handleDeleteUser(user.id)}
                  className="item-admin-table-button"
                >
                  <img src={deleteButton} />
                </button>
              </div>
            </div>
          </div>
        ))}

        {showPopup && (
          <div className="popup">
            <div className="popup-inner">
              <button
                className="close-popup"
                onClick={() => setShowPopup(false)}
              >
                X
              </button>
              <input
                type="text"
                name="username"
                placeholder="Username"
                value={formData.userName}
                onChange={handleInputChange}
                required
              />
              <input
                type="text"
                name="password"
                placeholder="Password"
                value={formData.password}
                onChange={handleInputChange}
                required
              />
              <input
                type="text"
                name="name"
                placeholder="Name"
                value={formData.name}
                onChange={handleInputChange}
                required
              />
              <input
                type="email"
                name="email"
                placeholder="Email"
                value={formData.email}
                onChange={handleInputChange}
                required
              />
              {/* <select
                name="form"
                value={formData.form}
                onChange={handleInputChange}
                required
              >
                <option value="">Select form</option>
                {channelData.map((val) => (
                  <option value="Form1">{val.label}</option>
                ))}
              </select> */}
              <input
                required
                style={{ cursor: "pointer" }}
                type="text"
                onFocus={(e) => (e.target.type = "date")}
                onBlur={(e) => {
                  if (e.target.value === "") {
                    e.target.type = "text";
                    e.target.placeholder = "Select expiry date";
                  }
                }}
                name="expiry"
                placeholder="Select expiry date"
                value={formData.expiry}
                onChange={handleInputChange}
                pattern="\d{4}-\d{2}-\d{2}"
              />

              <MultiSelect
                options={channelData.map((val) => val.label)}
                selectedOptions={formData.radioLinks}
                setSelectedOptions={(val: React.SetStateAction<string[]>) =>
                  setFormData({ ...formData, radioLinks: val as string[] })
                }
              />
              {/* <select
                name="login"
                value={formData.login}
                onChange={handleInputChange}
                required
              >
                <option value="">Select login</option>
                <option value="Login 1">Login 1</option>
                <option value="Login 2">Login 2</option>
                <option value="Login 3">Login 3</option>
                <option value="Login 4">Login 4</option>
                <option value="Login 5">Login 5</option>
                <option value="Login 6">Login 6</option>
              </select> */}

              <div className="item-admin-table-buttons">
                <button onClick={handleAddUser}>Добавить</button>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Admin;
