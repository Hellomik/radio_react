// import { ObjectId } from 'mongodb';

export interface ChannelEntity {
  _id: string;
  label: string;
  link: string;
}
