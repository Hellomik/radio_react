import React, { useEffect, useRef, useState } from "react";
import "./index.css";
import logo from "../../assets/hotcoffee.png";
import phoneWidget from "../../assets/phone-widget.png";
import { useLocation, useParams } from "react-router";

import axios from "axios";
import { globalHost } from "../..";
import { ChannelEntity } from "../channel_entity";
import { UserData } from "../Admin";
import Popup from "reactjs-popup";

function Home() {
  const { login } = useParams();
  const [channelData, setChannelData] = useState<ChannelEntity[]>([]);
  const [channelDataMap, setChannelMap] = useState<
    Record<string, ChannelEntity>
  >({});
  // const [token, setToken] = useState<string | null>();
  const tokenRef = useRef<string | null>(null);
  const [userData, setUserData] = useState<UserData | null>();

  const [musicSrc, setMusicSrc] = useState<string | null>();

  const [blocked, setBlock] = useState<boolean>(false);

  useEffect(() => {
    const getChannels = async () => {
      try {
        const link = globalHost + "/channel";
        const responce = await axios.get(link);
        const newRecordChannel: Record<string, ChannelEntity> = {};

        (responce.data as ChannelEntity[]).forEach((val) => {
          newRecordChannel[val.label] = val;
        });

        setChannelMap(newRecordChannel);

        setChannelData(responce.data);
      } catch (e) {
        console.log(e);
      }
    };

    const getUserData = async () => {
      const link = globalHost + "/user";
      const responce = await axios.get(link);
      const userData: UserData = (responce.data as UserData[]).find((val) => {
        return val.userName == login;
      })!;
      setUserData(userData);
    };

    getChannels();
    getUserData();
  }, []);

  useEffect(() => {
    let finished = false;
    const doFunction = async () => {
      if (finished) return;
      console.log("TOKEN USEd", tokenRef.current);
      const repsonceToken = await axios.post(
        "http://localhost:3007/usercheck",
        {
          token: tokenRef.current,
          userName: login,
        }
      );
      if (repsonceToken.data == "" || repsonceToken.data == null) {
        const audioPlayer = document.getElementById(
          "audio"
        ) as HTMLAudioElement | null;
        audioPlayer?.pause();
        setBlock(true);
      } else {
        setBlock(false);
        // setToken(repsonceToken.data);
        tokenRef.current = repsonceToken.data;
        console.log("GOT TOKEN", repsonceToken.data);
      }
      setTimeout(() => doFunction(), 1000);
    };
    setTimeout(() => doFunction(), 1000);
    return () => {
      finished = true;
    };
  }, []);

  const allMyChannels =
    userData?.radioLinks?.map((val) => channelDataMap[val]) ?? [];
  var arrays: ChannelEntity[][] = [],
    size = 2;
  while (allMyChannels.length > 0) arrays.push(allMyChannels.splice(0, size));

  return (
    <div className="home-container">
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>

      <div className="audio-container">
        {musicSrc && (
          <audio
            id="audio"
            className="audio-player"
            controls
            src={musicSrc}
          ></audio>
        )}
      </div>
      {arrays.map((valParent) => (
        <div className="janre-selector-container">
          {valParent.map((val) => (
            <div
              onClick={() => {
                setMusicSrc(val?.link);
              }}
              className="janre-selector"
            >
              {val?.label}
            </div>
          ))}
        </div>
      ))}

      <div className="phone-widget">
        <img src={phoneWidget} alt="phone-widget" />
      </div>

      <div className="header-home">
        <div className="menu-user">{userData?.userName}</div>
        <button className="menu-logout">Logout</button>
      </div>
      {blocked && (
        <div className="bloc-continaer">
          <div className="bloc-inner">
            На данный момент ваш аккаунт используется уже другим пользователем
            <br />
            {/* {token} */}
            Попросите его/ее выйти и убедитесь что нету вкладок с такой же
            ссылкой
          </div>
        </div>
      )}
    </div>
  );
}

export default Home;
